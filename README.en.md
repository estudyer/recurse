# recurse

#### Introduction
Array recursive/non-recursive processing.

#### Installation

composer require markg/recurse

#### Usage
// Default configuration
$option = [
	// Primary key [default is 'id' if empty]
	'primary_field' => 'id',
	// Parent key name
    'parent_field' => 'pid',
    // Selected state field name [no return if not set]
    'select_field' => '',
    // Child key name in the returned data [must be set together with parent_field for infinite recursion, otherwise it's a normal foreach]
    'child_key' => 'child',
    // Selected state key name in the returned data [must be set together with select_field to return selected state]
    'select_key' => 'select',
    // ...... // Other custom parameters, can be used in callback through $option
    // $option->option['custom_param_key']
    // $option->custom_param_key
];
$opt = new \Markg\Recurse\Option($option)
// The callback set will be executed when each recurse item finishes processing
$opt->setItemCallback(function($item, $index, $option) {
	//:TODO Custom processing
});

// $option can be omitted and default configuration will be used
$recurse = \Markg\Recurse\Recurse::create($option);
// $recurse = \Markg\Recurse\Recurse::create($option, $array);
// $recurse = new \Markg\Recurse\Recurse($option);
// $recurse = new \Markg\Recurse\Recurse($option, $array);
// $recurse->data($array); // Reset/Set data

// No need to return selected state [conditions in $option must be met]
$recurse->recurse($array, $parentVal);
// Parent value defaults to 0
// $recurse->recurse($array);
// When $array has been set, $array can be an empty array [] or not passed
// $recurse->recurse();
// Need to return selected state [conditions in $option must be met]
// $recurse->recurse($array, $parentVal, $selected);

// Recursive without specifying a parent
$recurse->shuffle($array, $selected); // $array, $selected are the same as in recurse