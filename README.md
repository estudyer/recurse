# recurse

#### 介绍
数组递归/不递归处理

#### 安装教程

composer require markg/recurse

#### 使用说明
// 默认配置
$option = [
	// 主键[为空时默认为id]
	'primary_field' => 'id',
	// 父级键名
    'parent_field' => 'pid',
    // 选中状态判断的键名[不设置不返回选中状态]
    'select_field' => '',
    // 返回数据中子集的键名[必须和parent_field同时设置才会进行无限递归处理，否则就是个正常的foreach]
    'child_key' => 'child',
    // 返回数据中选中状态的键名[必须和select_field同时设置才会返回选中状态]
    'select_key' => 'select',
    // ...... // 其他自定义参数，可以在callback中的$option里使用。两种使用方式都可以
    // $option->option['自定义参数key']
    // $option->自定义参数key
];
$opt = new \Markg\Recurse\Option($option)
// 当recurse处理每一条结束的时候会调用设置的callback
$opt->setItemCallback(function($item, $index, $option) {
	//:TODO 自定义处理
});

// $option 可以不设置，会使用默认配置
$recurse = \Markg\Recurse\Recurse::create($option);
// $recurse = \Markg\Recurse\Recurse::create($option, $array);
// $recurse = new \Markg\Recurse\Recurse($option);
// $recurse = new \Markg\Recurse\Recurse($option, $array);
// $recurse->data($array); // 重置/设置数据

// 不需要返回选中状态[$option中的条件要满足]
$recurse->recurse($array, $parentVal);
// 父级值默认为0
// $recurse->recurse($array);
// 当已经设置过$array之后$array可以传空数组[]或不传
// $recurse->recurse();
 // 需要返回选中状态[$option中的条件要满足]
// $recurse->recurse($array, $parentVal, $selected);


// 无指定父级递归
$recurse->shuffle($array, $selected); // $array、$selected和recurse中的一样

