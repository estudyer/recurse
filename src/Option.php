<?php
declare(strict_types=1);
namespace Markg\Recurse;

/**
 * @package Markg\Recurse
 */
class Option {
    /**
     * @var array
     */
    public $option = [
        'primary_field' => 'id',
        'parent_field' => 'pid',
        'select_field' => '',
        'child_key' => 'child',
        'select_key' => 'select'
    ];

    /**
     * @var string
     */
    public $primary_field = 'id';

    /**
     * @var string
     */
    public $parent_field = 'pid';

    /**
     * @var string
     */
    public $select_field = '';

    /**
     * @var string
     */
    public $child_key = 'child';

    /**
     * @var string
     */
    public $select_key = 'select';

    /**
     * @var \Closure|null
     */
    public $item_callback = null;

    /**
     * @param array $option
     */
	public function __construct(array $option = []) {
        $this->option = array_merge($this->option, $option);

        $this->setOptionProperty();
	}

    private function setOptionProperty() {
        $this->primary_field = (string) $this->option['primary_field'] ?: 'id';
        $this->parent_field = (string) $this->option['parent_field'];
        $this->select_field = (string) $this->option['select_field'];
        $this->child_key = (string) $this->option['child_key'];
        $this->select_key = (string) $this->option['select_key'];
    }

    /**
     * @param \Closure $callback
     * @return $this
     */
    public function setItemCallback(\Closure $callback) {
        $this->item_callback = $callback;

        return $this;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get(string $name)
    {
        return $this->option[$name] ?? null;
    }
}