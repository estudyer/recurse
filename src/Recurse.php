<?php
declare(strict_types=1);
namespace Markg\Recurse;

/**
 * Recurse
 */
class Recurse
{
    /**
     * @var Option|null
     */
    private $option = null;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @param Option|null $option
     * @param array $data
     */
    public function __construct(Option $option = null, array $data = [])
    {
        if (null === $option) $option = new Option();

        $this->option = $option;
        $this->data($data);
    }

    /**
     * @param Option|null $option
     * @param array $data
     * @return self
     */
    public static function create(Option $option = null, array $data = []) {
        return new self($option, $data);
    }

    /**
     * @param array $data
     * @return $this
     */
    public function data(array $data) {
        $this->data = $data;

        return $this;
    }

    /**
     * @param array $data
     * @param int|string $parent
     * @param array $select
     * @return array
     */
    public function recurse(array $data = [], $parent = 0, array $select = []) {
        $data = empty($data) ? $this->data : $data;
        $array = [];

        foreach ($data as $index => $item) {
            if ($this->option->select_field && $this->option->select_key) {
                $item[$this->option->select_key] = in_array($item[$this->option->select_field], $select) ? 1 : 2;
            }

            if ($this->option->child_key && $this->option->parent_field) {
                if ($item[$this->option->parent_field] === $parent) {
                    $item[$this->option->child_key] = $this->recurse($data, $item[$this->option->primary_field], $select);
                    if ($this->option->item_callback instanceof \Closure) {
                        $item = ($this->option->item_callback)($item, $index, $this->option);
                    }

                    $array[] = $item;
                }
            } else {
                if ($this->option->item_callback instanceof \Closure) {
                    $item = ($this->option->item_callback)($item, $index, $this->option);
                }
                $array[] = $item;
            }
        }

        return $array;
    }

    /**
     * @param array $data
     * @param array $select
     * @return array
     */
    public function shuffle(array $data = [], array $select = []) {
        $data = array_column(empty($data) ? $this->data : $data, null, $this->option->primary_field);
        $parents = [];

        foreach ($data as $index => $item) {
            if (!isset($data[$item[$this->option->parent_field]])) {
                $parents[] = $item[$this->option->parent_field];
            }
        }

        $parents = array_values(array_unique($parents));
        $data = array_values($data);
        $array = [];
        foreach ($parents as $parent) {
            $array = array_merge($array, $this->recurse($data, $parent, $select));
        }

        return $array;
    }
}